from tree_sitter import Language, Parser
from git.repo import Repo
# import sh
import os


def instantiate_language(name, url):
    # Download language definition
    if os.path.isfile(f"./{name}.so"):
        return Language(f"./{name}.so", name)

    # sh.git.clone(url, name)
    try:
        Repo.clone_from(url, name)
    except:
        pass
    # Compile language definition
    # NOTE: It is also possible to compile multiple languages in a single library.
    Language.build_library(f"{name}.so", [name])
    # Load language
    return Language(f"./{name}.so", name)


PY = instantiate_language(
    "python", "https://github.com/tree-sitter/tree-sitter-python")
C = instantiate_language(
    "c", "https://github.com/tree-sitter/tree-sitter-c")

# Instantiate parser
parser = Parser()


parser.set_language(C)

my_dict = {}
target_dir = "./build/_deps/dynamorio-src/samples/"


def traverse(node):
    if node.type == "call_expression":
        # print(node)
        function_name = node.child_by_field_name('function').text
        if my_dict.get(function_name) is None:
            my_dict[function_name] = 1
        else:
            my_dict[function_name] += 1

    for n in node.children:
        traverse(n)


for filename in os.listdir(target_dir):
    print(filename)
    if os.path.isdir(target_dir + filename):
        continue
    if filename[-2:] != ".c":
        continue

    with open(target_dir + filename, 'br') as f:
        my_source = f.read()
        tree = parser.parse(my_source)
        root = tree.root_node

        traverse(root)

tmp_dict = sorted(my_dict.items(), key=lambda x: x[1], reverse=True)
# print(tmp_dict)
for i in range(10):
    print(tmp_dict[i])
